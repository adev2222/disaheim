﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disaheim2
{
    public class Controller
    {
        public ValuableRepository ValuableRepository = new ValuableRepository();

        public Controller() { }
        

        public void AddToList(IValuable valuable)
        {
            ValuableRepository.AddValuable(valuable);
        }
    }
}
