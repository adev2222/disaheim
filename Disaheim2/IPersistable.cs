namespace Disaheim2;

public interface IPersistable
{
     void Save(string fileName = "ValuableRepository.txt");
     void Load(string fileName = "ValuableRepository.txt");
}