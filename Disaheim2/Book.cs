﻿namespace Disaheim2
{
    public class Book : Merchandise
    {
 
        public string Title { get; set; }
        public double Price { get; set; }


        public Book(string itemId) : base(itemId){}

        public Book(string itemId, string title): this(itemId)
        {
            Title = title;
        }

        public Book(string itemId,string title, double price) :this(itemId, title)
        {
            Price = price;
        }


        public override string ToString()
        {
            return $"ItemId: {ItemId}, Title: {Title}, Price: {Price}";
        }

        public override double GetValue()
        {
            return Price;
        }
    }
}
