﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disaheim2
{
    public abstract class Merchandise : IValuable
    {
        public string ItemId { get; set; }

        public Merchandise(string itemId) => ItemId = itemId;

        public abstract override string ToString();

        public abstract double GetValue();
    }
}
