namespace Disaheim2
{
    public class ValuableRepository : IPersistable
    {
        private List<IValuable> _valuables = new List<IValuable>();

        public void AddValuable(IValuable valuable)
        {
            _valuables.Add(valuable);
        }

        public IValuable GetValuable(string id)
        {
            return _valuables.Find(x => x.ToString().Contains(id));
        }

        public double GetTotalValue()
        {
            return _valuables.Sum(x => x.GetValue());
        }

        public int Count()
        {
            return _valuables.Count();
        }

        public void Save(string fileName = "ValuableRepository.txt")
        {
            using var writer = new StreamWriter(fileName, true);
            _valuables.ForEach(x =>
            {
                switch (x)
                {
                    case Book book:
                        writer.WriteLine($"{x.GetType().Name};{book.ItemId};{book.Title};{book.GetValue()}");
                        break;
                    
                    case Amulet amulet:
                        writer.WriteLine($"{x.GetType().Name};{amulet.ItemId};{amulet.Design};{amulet.Quality}");
                        break;
                    
                    case Course course:
                        writer.WriteLine($"{x.GetType().Name};{course.Name};{course.DurationInMinutes}");
                        break;
                }
            });
        }

        public void Load(string fileName = "ValuableRepository.txt")
        {
            try
            {
                using var reader = new StreamReader(fileName);
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine()?.Split(";");
                    if (line == null || line.Length < 2) continue;
                
                    switch (line[0])
                    {
                        case "Book":
                            var book = new Book(line[1]);
                            if (line.Length > 2) book.Title = line[2];
                            if (line.Length > 3) book.Price = Convert.ToDouble(line[3]);
                            _valuables.Add(book);
                            break;
                        
                        case "Amulet":
                            var amulet = new Amulet(line[1]);
                            if (line.Length > 2) amulet.Design = line[2];
                            if (line.Length > 3)
                                amulet.Quality = line[3] switch
                                {
                                    "high" => Level.high,
                                    "low" => Level.low,
                                    _ => Level.medium
                                };
                            _valuables.Add(amulet);
                            break;
                        
                        case "Course":
                            var course = new Course(line[1]);
                            if (line.Length > 2) course.DurationInMinutes = Convert.ToInt32(line[2]);
                            _valuables.Add(course);
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
