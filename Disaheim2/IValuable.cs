namespace Disaheim2;

public interface IValuable
{
    double GetValue();
}