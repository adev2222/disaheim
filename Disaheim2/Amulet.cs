﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Disaheim2
{
    public class Amulet : Merchandise
    {
        public string Design { get; set; }
        public Level Quality { get; set; } = Level.medium;
        public static double LowQualityValue { get; set; } = 12.5;
        public static double MediumQualityValue { get; set; } = 20.0;
        public static double HighQualityValue { get; set; } = 27.5;
        
        public Amulet(string itemId) : base(itemId)
        {
      
        }

        public Amulet(string itemId, Level quality) : this(itemId)
        {
            Quality = quality;
        }

        public Amulet(string itemId, Level quality, string design ) : this(itemId, quality)
        {
            Design = design;
        }


        public override string ToString()
        {
            return $"ItemId: {ItemId}, Quality: {Quality}, Design: {Design}";
        }

        public override double GetValue()
        {
            return Quality == Level.low ? LowQualityValue :
                Quality == Level.high ? HighQualityValue : MediumQualityValue;
            
          /*  if (Quality == Level.low) return LowQualityValue;
            else if (Quality == Level.high) return HighQualityValue;
            else return MediumQualityValue;*/
        }
    }
}
