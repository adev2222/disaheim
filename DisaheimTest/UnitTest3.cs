﻿using Disaheim2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DisaheimTest
{
    [TestClass]
    public class UnitTest3
    {
        Book b1, b2, b3;
        Amulet a1, a2, a3;
        Course c1, c2, c3;

        Controller controller;

      

        [TestInitialize]
        public void Init()
        {
            // Arrange

            b1 = new Book("1");
            b2 = new Book("2", "Falling in Love with Yourself");
            b3 = new Book("3", "Spirits in the Night", 123.55);

            a1 = new Amulet("11");
            a2 = new Amulet("12", Level.high);
            a3 = new Amulet("13", Level.low, "Capricorn");

            c1 = new Course("Spådomskunst for nybegyndere");
            c2 = new Course("Magi – når videnskaben stopper", 157);
            c3 = new Course("Et indblik i Helleristning", 180);


            controller = new Controller();

            controller.AddToList(b1);
            controller.AddToList(b2);
            controller.AddToList(b3);

            controller.AddToList(a1);
            controller.AddToList(a2);
            controller.AddToList(a3);


            controller.AddToList(c1);
            controller.AddToList(c2);
            controller.AddToList(c3);

     
        }

        [TestMethod]
        public void TestBookList()
        {
            // Assert
            Assert.AreEqual(b3, controller.ValuableRepository.GetValuable(b3.ItemId));
        }

        [TestMethod]
        public void TestAmuletList()
        {
            // Assert
            Assert.AreEqual(a1, controller.ValuableRepository.GetValuable(a1.ItemId));
        }
        [TestMethod]
        public void TestCourseList()
        {
            // Assert
            Assert.AreEqual(c1, controller.ValuableRepository.GetValuable(c1.Name));
            Assert.AreEqual(c2, controller.ValuableRepository.GetValuable(c2.Name));
            Assert.AreEqual(c3, controller.ValuableRepository.GetValuable(c3.Name));
        }
        
    }


}
